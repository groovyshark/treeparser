#include "MyTree.h"

std::string TreeNode::toTable(char delimeter, std::stringstream& sout)
{
	std::string val;
	//���� ���� �������, �� �������� ��� �� ������� ��� ���� �����
	if (!leaf)
	{
		std::stringstream oss;
		oss << "{";
		std::for_each(children.begin(), children.end(), [&](const NodePtr& node)
		{
			oss << node->name << " = " << node->toTable(delimeter, sout) << " ";
		});
		oss << "}";

		val = oss.str();
	}
	//���� ���, ���������� ������� �������� ����
	else 
	{
		val = value;
	}

	//���������� �������������� ������
	sout << id << ", " << idParent << ", " << name << ", " << val << std::endl;

	return val;

}
NodePtr TreeNode::findNode(std::string &src)
{
	int pos;
	static int _id = 1;
	if ((pos = src.find_first_of('=')) < 0) //���� �� ������� ����� - �������
	{
		return NULL;
	}

	if (src.find_first_of('}') < pos) // ����������, ��� ����� �� ������
	{
		return NULL;
	}

	//���������� ��, ��� �� � ����� ����� �����
	std::string nodeName = src.substr(0, pos - 1);
	std::string tail = src.substr(pos + 2, src.size());

	//���������� ������� ������ ������� ��� ������
	int posQuote = tail.find_first_of('\"');
	int posBraket = tail.find_first_of('{');

	int minPos = posBraket < 0 ? posQuote :
		posQuote < 0 ? posBraket :
		std::min(posQuote, posBraket);
	if (minPos < 0)
		throw std::runtime_error("�������� ������ �����");

	bool isLeaf = (minPos == posQuote) ? true : false;

	//������� ������ �������
	if (minPos - pos > 1)
	{
		auto strGap = src.substr(pos, minPos - pos);
		if (strGap.find_first_not_of(' ') != strGap.size())
			throw std::runtime_error("�������� ������ �����");
	}
	int posSpace = nodeName.find_first_not_of(' ');


	nodeName = nodeName.substr(posSpace, nodeName.find_last_not_of(' ') - posSpace + 1);

	if (isdigit(nodeName[0]))
	{
		throw std::runtime_error("�������� ������ �����");
	}

	NodePtr node;
	//���� ����, ��������� �������� ���� � �������� �����
	if (isLeaf) 
	{
		std::string nodeVal = tail.substr(minPos + 1, tail.find_first_of('\"', minPos + 1) - minPos - 1);

		std::size_t found = nodeVal.find_first_of("\"\n");
		if (found != std::string::npos)
		{
			throw std::runtime_error("�������� ������ �����");
		}

		int p = tail.find_first_of('\"', minPos + 1) + 1;
		int np = tail.size() + 1 - p;

		tail = tail.substr(p, np);

		node.reset(new TreeNode(id + children.size() + 1, id, isLeaf, nodeName, nodeVal));
	}
	//���� �� ����, �������� ������� �������� ��� �����
	else 
	{
		node.reset((new TreeNode(id + children.size() + 1, id, isLeaf, nodeName)));

		NodePtr chNode;
		tail = tail.substr(1);
		while (chNode = node->findNode(tail))
		{
			node->children.push_back(chNode);
		}

		//�������� ����� ����� ������
		int posBr = tail.find_first_of('}');
		tail = tail.substr(posBr + 1, tail.size() - posBr);
	}
	src = tail;

	return node;
}