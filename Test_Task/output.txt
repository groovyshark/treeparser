2, 1, type, tetrahedron
5, 4, x, 1
6, 4, y, 0
7, 4, z, 0
4, 3, point, {x = 1 y = 0 z = 0 }
6, 5, x, 0
7, 5, y, 1
8, 5, z, 0
5, 3, point, {x = 0 y = 1 z = 0 }
7, 6, x, 0
8, 6, y, 0
9, 6, z, 1
6, 3, point, {x = 0 y = 0 z = 1 }
8, 7, x, 1
9, 7, y, 1
10, 7, z, 1
7, 3, point, {x = 1 y = 1 z = 1 }
3, 1, vertices, {point = {x = 1 y = 0 z = 0 } point = {x = 0 y = 1 z = 0 } point = {x = 0 y = 0 z = 1 } point = {x = 1 y = 1 z = 1 } }
5, 4, r, 0xFF
6, 4, g, 0x00
7, 4, b, 0x80
8, 4, alpha, 0x80
4, 1, color, {r = 0xFF g = 0x00 b = 0x80 alpha = 0x80 }
1, 0, shape, {type = tetrahedron vertices = {point = {x = 1 y = 0 z = 0 } point = {x = 0 y = 1 z = 0 } point = {x = 0 y = 0 z = 1 } point = {x = 1 y = 1 z = 1 } } color = {r = 0xFF g = 0x00 b = 0x80 alpha = 0x80 } }
