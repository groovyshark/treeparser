#ifndef MYTREE_H
#define MYTREE_H

#include <string>
#include <vector>
#include <fstream>
#include <regex>
#include <sstream>
#include <memory>
#include <stdexcept>

class Tree;
class TreeNode;

typedef std::shared_ptr <TreeNode> NodePtr;

/// ����� ���� ������
/*
 * ����� �������� ������ � ���������� ����� � ������
 */
class TreeNode
{
private:
	unsigned int id;									///< ID ����
	unsigned int idParent;								///< ID ������������ ����
	bool leaf;											///< ����-�������� �����
	std::string name;									///< ��� ����
	std::string value;									///< �������� � ����
	std::vector <NodePtr> children;						///< ����
public:
	TreeNode() :
		id(0),
		idParent(0),
		leaf(true)
	{
	}

	TreeNode(unsigned int _id, unsigned int _idParent, bool _leaf,
		 const std::string& _name, const std::string& _value = "") :
		id(_id),
		idParent(_idParent),
		leaf(_leaf),
		name(_name),
		value(_value)
	{}

	//! ������� ������������ ��������� �������
	/*!
	  \������ �������� - ��� �����������
	  \������ �������� - ��������� �����
	  \���������� ��������������� ������
	*/
	std::string toTable(char delimeter, std::stringstream& sout); 

private:
	//! ������� �������� � ���������� ����
	/*!
	  \������������ �������� - ������
	  \���������� ����
	*/
	NodePtr findNode(std::string &);

	friend class Tree;
};


///����� ������
/*
 * �����, �������� ������ ���������
 */
class Tree
{
private:
	NodePtr root;
	std::string srcString;
public:

	//!�����������
	/*!
	  \��������� �������� ������
	  \� ��� ����� ���������� ������� ������
	  \���������� ��������� �� ������
	*/
	Tree(const std::string &src) :
		srcString(src)
	{
		TreeNode tempRoot;
		root = tempRoot.findNode(srcString);

		std::size_t found = srcString.find_first_not_of(" \n\t\r");

		if (found != std::string::npos)
		{
			throw std::runtime_error("�������� ������ �����");
		}
	}

	//! ������������ �������
	/*!
	  \��������� ��� �����������
	  \���������� ��� ������ � ������ �������
	*/
	std::string toTable(char delimeter = ',')
	{
		std::stringstream sout;
		root->toTable(delimeter, sout);
		return sout.str();
	}

};

#endif //MYTREE_H